require 'open3'

Jekyll::Hooks.register :site, :post_write, priority: :low do |site|
  cmd = ['pagefind', '--force-language', site.config['locale'], '--site', site.dest, '--root-selector', 'article']
  Open3.popen2e(*cmd, unsetenv_others: true) do |_, o, t|
    Thread.new do
      o.each do |line|
        Jekyll.logger.info 'Pagefind:', line
      end
    rescue IOError => e
      Jekyll.logger.info 'Pagefind:', e.message
    end

    unless t.value.success?
      Jekyll.logger.info 'Pagefind:', 'There was an error'
    end
  end
end
