# frozen_string_literal: true

Jekyll::PageWithoutAFile.class_eval do
  def date
    data['date']
  end
end

require 'nanoid'

Jekyll::Hooks.register :site, :post_read do |site|
  Jekyll.logger.info 'Anexos:', 'Procesando'
  suspendida_re = /\(suspendida\)/i

  site.data['charts'] ||= {}
  site.data['charts']['resumen'] = {
    'Listadas' => site.data['base_completa'].size,
    'Suspendidas' => 0,
    'Retiro de autorización' => 0,
  }

  # Procesa la información en la otra tabla. Queremos agrupar por tipo y
  # matrícula y sacar en qué anexo están.
  #
  # @todo Soportar consultas SQLite :P
  bajas = {}
  site.data['bajas'].each_pair.map do |anexo, data|
    tipo = site.data.dig('anexo', anexo, 'tipo')
    bajas[tipo] ||= {}
    bajas[tipo].merge!(data.map do |d|
      [
        d['Matricula'],
        anexo
      ]
    end.to_h)
  end

  coops = {}

  site.data['base_completa'].each_with_index do |fila, i|
    razon_social = fila['Razón Social'].sub(suspendida_re, '')
    suspendida = razon_social != fila['Razón Social']
    razon_social = razon_social.tr("\n", ' ').squeeze(' ').strip
    anexo = fila['Anexo']

    if suspendida
      site.data['charts']['resumen']['Suspendidas'] += 1
    end

    Jekyll::PageWithoutAFile.new(site, '', "cooperativas/#{anexo}", "#{i}-#{Jekyll::Utils.slugify(razon_social[..100])}.md").tap do |coop|
      site.posts.docs << coop

      coops[fila['CUIT'] || fila['Matrícula']] ||= coop

      coop.data = {
        'uuid' => Nanoid.generate,
        'layout' => 'cooperativa',
        'title' => razon_social,
        'date' => Date.today,
        'matricula' => fila['Matrícula'],
        'cuit' => fila['CUIT'],
        'anexo' => anexo,
        'provincia' => fila['Provincia'],
        'suspendida' => suspendida,
        'retiro' => bajas.dig(site.data.dig('anexo', anexo, 'tipo'), fila['Matrícula'])
      }

      retiro = site.data.dig('anexo', coop.data['retiro'], 'instructivo')

      unless retiro.nil? || retiro.empty?
        site.data['charts']['resumen']['Retiro de autorización'] += 1
      end
    end
  end

  site.data['2025_suspendidas'].each_with_index do |fila, i|
    identificador = fila['CUIT'] || fila['Matricula']
    coop = coops[identificador]
    razon_social = fila['Razón Social']
    anexo = '2025'

    unless coop
      Jekyll.logger.warn "Anexos:", "#{razon_social} no existe"

      coop = Jekyll::PageWithoutAFile.new(site, '', "cooperativas/#{anexo}", "#{i}-#{Jekyll::Utils.slugify(razon_social[..100])}.md").tap do |coop|
        site.posts.docs << coop

        coops[identificador] ||= coop
        site.data['charts']['resumen']['Listadas'] += 1
        site.data['charts']['resumen']['Suspendidas'] += 1

        coop.data = {
          'uuid' => Nanoid.generate,
          'layout' => 'cooperativa',
          'title' => razon_social,
          'date' => Date.today,
          'matricula' => fila['Matricula'],
          'cuit' => fila['CUIT'],
          'provincia' => fila['Provincia']
        }
      end
    end

    coop.data['suspendida'] = true
    coop.data['anexo'] = 'IF-2024-134994844-APN-DNCYF_INAES'
    coop.data['retiro'] = 'IF-2024-134994844-APN-DNCYF_INAES'
  end

  site.data['charts']['resumen'] = site.data['charts']['resumen'].to_a
  Jekyll.logger.info 'Anexos:', 'Procesados'
end
