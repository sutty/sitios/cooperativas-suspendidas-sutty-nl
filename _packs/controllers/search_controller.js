import { Controller } from "@hotwired/stimulus";

import { createDbWorker } from "sql.js-httpvfs";

export default class extends Controller {
  static targets = ["q", "container", "more", "showAfter"];
  static values = {
    stopWords: Array,
    q: String,
    l: {
      type: Number,
      default: 10,
    },
    o: {
      type: Number,
      default: 0,
    },
  };

  get q() {
    if (!this.hasQTarget) return;
    if (!this.qTarget.value.trim().length === 0) return;

    this.qValue = this.qTarget.value.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replaceAll(/[:~\*\^\+\-]/gi, "").toLowerCase().split(" ").filter(x => !this.stopWordsValue.includes(x)).join(" ");
    return this.qValue;
  }

  async connect() {
    const searchParams = new URLSearchParams(window.location.search);
    this.qValue = searchParams.get("q")?.trim();
    this.lValue = parseInt(searchParams.get("l")?.trim()) || 10;

    const config = {
      from: "inline",
      config: {
        serverMode: "full",
        requestChunkSize: 4096,
        url: "/assets/sqlite/index.sqlite3",
      },
    };

    const maxBytesToRead = 10 * 1024 * 1024;
    window.worker = await createDbWorker(
      [config],
      "/assets/js/sqlite.worker.js",
      "/assets/wasm/sql-wasm.wasm",
      maxBytesToRead
    );

    if (this.qValue) {
      this.qTarget.value = this.qValue;
      this.search();
    }
  }

  reset(event) {
    this.indexedResults = null;
    this.oValue = 0;
    this.lValue = 10;
    this.showAfterTargets.forEach(x => x.classList.add("d-none"));
  }

  async search(event) {
    const isNumeric = /^\d+$/;

    // Detiene el envío del formulario
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.formDisable = true;
    this.moreTarget.classList.add("d-none");
    this.showAfterTargets.forEach(x => x.classList.remove("d-none"));

    // Obtiene el término de búsqueda
    const q = this.q;

    this.containerTarget.innerHTML = "<p>Buscando...</p>";
    let query;
    let count = 0;

    if (isNumeric.test(q)) {
      count = (await window.worker.db.exec(
        "select count(*) from data where cuit = ? or matricula = ?",
        [q, q]
      ))[0].values[0][0];
      query = await window.worker.db.exec(
        "select url as id from data where cuit = ? or matricula = ?",
        [q, q]
      );
    } else {
      count = (await window.worker.db.exec(
        "select count(*) from indexed_posts(?)",
        [q]
      ))[0].values[0][0];
      query = await window.worker.db.exec(
        "select url as id from indexed_posts(?) left join data on data.id = indexed_posts.rowid order by rank",
        [q]
      );
    }

    document.title = this.qTarget.value;
    this.updateHistory();

    this.containerTarget.innerHTML = "";

    if (query.length === 0) {
      this.containerTarget.innerHTML = `<p>No encontramos "${this.qTarget.value}" en la base de datos de cooperativas y mutuales suspendidas. <strong>Para asegurarte que no sea por error:</strong></p><ul><li>Si buscaste por CUIT, intentá buscando por matrícula o nombre.</li><li>Si buscaste por nombre, intentá con el nombre completo, sin términos comunes como "cooperativa", "trabajo", etc.</li></ul>`;
    } else {
      this.indexedResults = query[0].values.map(x => ({ id: x[0] }));
      const firstResults = this.indexedResults.slice(this.oValue, this.lValue);
      this.renderResults(firstResults);

      if (firstResults.length < count) {
        this.moreTarget.classList.remove("d-none");
      }
    }

    this.formDisable = false;
  }

  renderResult(result) {
    return fetch(result.id).then(response => response.text()).then(body => this.containerTarget.insertAdjacentHTML("beforeend", body));
  }

  renderResults(results) {
    results.forEach(result => this.renderResult(result));
  }

  more(event = undefined) {
    event?.preventDefault();
    event?.stopPropagation();

    this.oValue = this.lValue;
    this.lValue = this.lValue + 10;
    this.renderResults(this.indexedResults.slice(this.oValue, this.lValue));
    this.updateHistory();
  }

  lValueChanged(newValue, oldValue) {
    if (!this.indexedResults) return;

    if (newValue < this.indexedResults.length) {
      this.moreTarget.classList.remove("d-none");
    } else {
      this.moreTarget.classList.add("d-none");
    }
  }

  updateHistory() {
    const q = this.qTarget.value;
    const l = this.lValue;
    const query = new URLSearchParams({ q, l });

    window.history.pushState({ q }, q, `?${query.toString()}`);
  }

  set formDisable(disable) {
    Array.from(this.element.elements).forEach((x) => (x.disabled = disable));
  }
}
