import { Controller } from "@hotwired/stimulus";
window.pagefind = require("../pagefind.js");

export default class extends Controller {
  static targets = ["q", "container", "more", "showAfter"];
  static values = {
    q: String,
    l: {
      type: Number,
      default: 10,
    },
    o: {
      type: Number,
      default: 0,
    },
  };

  get q() {
    if (!this.hasQTarget) return;
    if (!this.qTarget.value.trim().length === 0) return;

    this.qValue = this.qTarget.value.trim();

    return this.qValue;
  }

  async connect() {
    await window.pagefind.options({
      baseUrl: "",
      bundlePath: "pagefind/",
      excerptLength: 0
    });

    window.pagefind.init();

    const searchParams = new URLSearchParams(window.location.search);
    this.qValue = searchParams.get("q")?.trim();
    this.lValue = parseInt(searchParams.get("l")?.trim()) || 10;

    if (this.qValue) {
      this.qTarget.value = this.qValue;
      this.search();
    }
  }

  async search(event = undefined) {
    event?.preventDefault();
    event?.stopPropagation();

    this.formDisable = true;
    this.moreTarget.classList.add("d-none");
    this.showAfterTargets.forEach(x => x.classList.remove("d-none"));

    const q = this.q;

    this.containerTarget.innerHTML = "<p>Buscando...</p>";

    this.searchResults = await window.pagefind.search(q);
    const count = this.searchResults.unfilteredResultCount;

    document.title = this.qTarget.value;
    this.updateHistory();

    this.containerTarget.innerHTML = "";

    if (count) {
      const firstResults = await this.resolveResults(this.searchResults.results.slice(this.oValue, this.lValue));

      this.renderResults(firstResults);

      if (firstResults.length < count) {
        this.moreTarget.classList.remove("d-none");
      }
    } else {
      this.containerTarget.innerHTML = `<p>No encontramos "${this.qTarget.value}" en la base de datos de cooperativas y mutuales suspendidas. <strong>Para asegurarte que no sea por error:</strong></p><ul><li>Si buscaste por CUIT, intentá buscando por matrícula o nombre.</li><li>Si buscaste por nombre, intentá con el nombre completo, sin términos comunes como "cooperativa", "trabajo", etc.</li></ul>`;
    }

    this.formDisable = false;
  }

  /*
   * Resuelve los datos de todos los resultados
   *
   * @param results [Array<Result>]
   * @return [Promise]
   */
  async resolveResults(results) {
    return (await Promise.all(results.map(r => r.data())));
  }

  async reset(event) {
    this.searchResults = null;
    this.oValue = 0;
    this.lValue = 10;
    this.showAfterTargets.forEach(x => x.classList.add("d-none"));
  }

  updateHistory() {
    const q = this.qTarget.value;
    const l = this.lValue;
    const query = new URLSearchParams({ q, l });

    window.history.pushState({ q }, q, `?${query.toString()}`);
  }

  set formDisable(disable) {
    Array.from(this.element.elements).forEach((x) => (x.disabled = disable));
  }

  renderResults(results) {
    results.forEach(result => this.renderResult(result));
  }

  renderResult(result) {
    return fetch(result.url).then(response => response.text()).then(body => this.containerTarget.insertAdjacentHTML("beforeend", body));
  }

  async more(event = undefined) {
    event?.preventDefault();
    event?.stopPropagation();

    this.oValue = this.lValue;
    this.lValue = this.lValue + 10;
    this.renderResults(await this.resolveResults(this.searchResults.results.slice(this.oValue, this.lValue)));
    this.updateHistory();
  }
}
