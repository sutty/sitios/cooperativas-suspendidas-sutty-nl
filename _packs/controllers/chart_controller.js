import { Controller } from "@hotwired/stimulus";
import Chartkick from "chartkick";
import "chartkick/chart.js";

export default class extends Controller {
  static targets = [];
  static values = {
    type: String,
    dataset: Array
  };

  connect() {
  }

  datasetValueChanged(newValue) {
    switch (this.typeValue) {
      case "column":
        new Chartkick.ColumnChart(this.element, newValue);
        break;
    }
  }
}
