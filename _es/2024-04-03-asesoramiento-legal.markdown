---
title: Asesoramiento legal
description: Listado de cooperativas y estudios legales y contables que pueden ayudarles
  a regularizar
author: []
categories: []
tags: []
locales: []
draft: false
order: 9
layout: post
uuid: 5f0f4136-5160-4e49-950e-b55749f19d44
render_with_liquid: false
usuaries:
- 1
- 534
created_at: 2024-04-03 14:21:10.715050162 +00:00
last_modified_at: 2024-05-03 14:17:04.125552032 +00:00
---

<blockquote class="blockquote">
  <p style="text-align:start">La lista de profesionales disponible en este sitio web se proporciona únicamente con fines informativos y como un servicio para la comunidad. La inclusión de un profesional en esta lista se basa en la incorporación solicitada por cada individuo con el fin de ofrecer un directorio de contactos para aquellos que buscan servicios específicos.</p>
  <p style="text-align:start">Es importante destacar que la Asociación Civil TES y FACTTIC no realizan verificaciones sobre la idoneidad, la calidad o la capacidad de los profesionales listados. Por lo tanto, no podemos garantizar la exactitud, integridad o confiabilidad de la información proporcionada por estos profesionales ni hacemos ninguna representación sobre sus habilidades o calificaciones.</p>
</blockquote>
<p style="text-align:start"><strong>Para sumarse a este listado, escribirnos a contacto@sutty.nl</strong></p>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>Todo el país:</strong></p>
<ul>
  <li>
    <p style="text-align:start">GCAT Cooperativa de Trabajo Ltda.</p>
    <p style="text-align:start">Conformada por asociados al Colegio de Graduados en Cooperativismo y Mutualismo<br>
      Actividad: Asistencia integral a Cooperativas y Mutuales</p>
    <p style="text-align:start">Ámbito de Cobertura: Todo el país con referentes locales en cada provincia</p>
    <p style="text-align:start">Mail: <a href="mailto:gcat.coop@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">gcat.coop@gmail.com</a></p>
    <p style="text-align:start">Whatsapp: 54-9 11 57981914 / 64735679</p>
    <p style="text-align:start">Cr.Gabriel Coceres: Coordinador Impositivo- Contable</p>
    <p style="text-align:start">Lic. Eduardo Milner : Coordinador en Gestión, institucional y legal</p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>CABA:</strong></p>
<ul>
  <li>
    <p style="text-align:start">Cooperativa de Trabajo ECC Ltda.</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Jurisdicciones: Santa Fe, Córdoba y CABA.</p>
    <p style="text-align:start">Mail: <a href="mailto:info@estudiocoopcontable.coop.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">info@estudiocoopcontable.coop.ar</a></p>
  </li>
  <li>
    <p style="text-align:start">Estudio Contable Moyental</p>
    <p style="text-align:start">Mail: <a href="mailto:moyental@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">moyental@gmail.com</a></p>
  </li>
  <li>
    <p style="text-align:start">Dra. Viviana Castillo<br>
      Abogada</p>
    <p style="text-align:start">Cel. (011) <a href="tel:115021-1990" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">15 5021-1990</a></p>
  </li>
  <li>
    <p style="text-align:start">Gabriela Figueroa</p>
    <p style="text-align:start">Abogada de cooperativas y Mutuales de todo el país. Consultora en Comunicación, Género y Economía Social.</p>
    <p style="text-align:start">Contacto: <a href="mailto:abogadacoop.mut@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">abogadacoop.mut@gmail.com</a></p>
    <p style="text-align:start">Solo Whatsapp: <a href="tel:1166222610" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">1166222610</a> (de 9 a 17 hs de lunes a viernes)</p>
    <p style="text-align:start">Más info aquí: <a href="https://cgcym.org.ar/directorio-profesionales/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://cgcym.org.ar/directorio-profesionales/</a></p>
  </li>
  <li>
    <p style="text-align:start">Cdor Rodrigo Olgado</p>
    <p style="text-align:start">Consultora SCE</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Mail: <a href="mailto:rodrigoolgado@sceconsultora.com.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">rodrigoolgado@sceconsultora.com.ar</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>BUENOS AIRES:</strong></p>
<ul>
  <li>
    <p style="text-align:start">Dr. GONZALEZ PATRON NICOLAS</p>
    <p style="text-align:start">Contador Público</p>
    <p style="text-align:start">Mail: <a href="mailto:nicogp47@hotmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">nicogp47@hotmail.com</a><br>
      Celular <a href="tel:223-5561404" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">223-5561404</a></p>
  </li>
  <li>
    <p style="text-align:start">Dra. Viviana Castillo<br>
      Abogada</p>
    <p style="text-align:start">Cel. (011) <a href="tel:115021-1990" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">15 5021-1990</a></p>
  </li>
  <li>
    <p style="text-align:start">Lic Clarisa Valentukonis</p>
    <p style="text-align:start">La Plata, Buenos Aires</p>
    <p style="text-align:start">Tel: <a href="tel:2214888157" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">2214888157</a></p>
    <p style="text-align:start">Mail: <a href="mailto:Lic.clarisarv@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Lic.clarisarv@gmail.com</a></p>
  </li>
  <li>
    <p style="text-align:start">Cdor Rodrigo Olgado</p>
    <p style="text-align:start">Consultora SCE</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Mail: <a href="mailto:rodrigoolgado@sceconsultora.com.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">rodrigoolgado@sceconsultora.com.ar</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>CHACO:</strong></p>
<ul>
  <li>
    <p style="text-align:start">Cra. Patricia Pacheco</p>
    <p style="text-align:start">Celular: <a href="tel:3624711211" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">3624711211</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>CHUBUT:</strong></p>
<ul>
  <li>
    <p style="text-align:start">MARIA CECILIA MUÑOZ PORTALEZ - Contadora Pública Nacional</p>
    <p style="text-align:start">Provincias: Rio Negro y Chubut</p>
    <p style="text-align:start">Ciudades: El Bolson, Bariloche, Lago Puelo, Maiten, Cholila, El Hoyo, etc</p>
    <p style="text-align:start">Cel: <a href="tel:2944325387" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">2944325387</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>CÓRDOBA:</strong></p>
<ul>
  <li>
    <p style="text-align:start">Cooperativa de Trabajo Otra Córdoba</p>
    <p style="text-align:start">Mail: <a href="mailto:administracion@otracordoba.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">administracion@otracordoba.com</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<ul>
  <li>
    <p style="text-align:start">COOPERATIVA DE TRABAJO GESTIONAR LIMITADA<br>
      Contacto: <a href="tel:35841901710" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">35841901710</a><br>
      Mail: <strong><a href="mailto:gestionar.coop@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">gestionar.coop@gmail.com</a></strong></p>
  </li>
</ul>
<p style="text-align:start"></p>
<ul>
  <li>
    <p style="text-align:start">Cooperativa de Trabajo ECC Ltda.</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Jurisdicciones: Santa Fe, Córdoba y CABA.</p>
    <p style="text-align:start">Mail: <a href="mailto:info@estudiocoopcontable.coop.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">info@estudiocoopcontable.coop.ar</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>RÍO NEGRO:</strong></p>
<ul>
  <li>
    <p style="text-align:start">MARIA CECILIA MUÑOZ PORTALEZ - Contadora Pública Nacional</p>
    <p style="text-align:start">Provincias: Rio Negro y Chubut</p>
    <p style="text-align:start">Ciudades: El Bolson, Bariloche, Lago Puelo, Maiten, Cholila, El Hoyo, etc</p>
    <p style="text-align:start">Cel: <a href="tel:2944325387" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">2944325387</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<p style="text-align:start"><strong>SANTA FE:</strong></p>
<ul>
  <li>
    <p style="text-align:start">OFICOOP LTDA</p>
    <p style="text-align:start">Sitio: <a href="https://www.oficoopltda.coop.ar/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://www.oficoopltda.coop.ar/</a></p>
    <p style="text-align:start">Mail: <a href="mailto:consultasoficoop@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">consultasoficoop@gmail.com</a></p>
  </li>
</ul>
<p style="text-align:start"></p>
<ul>
  <li>
    <p style="text-align:start">Cooperativa de Trabajo ECC Ltda.</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Jurisdicciones: Santa Fe, Córdoba y CABA.</p>
    <p style="text-align:start">Mail: <a href="mailto:info@estudiocoopcontable.coop.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">info@estudiocoopcontable.coop.ar</a></p>
    <p style="text-align:start"></p>
  </li>
  <li>
    <p style="text-align:start">Abogado Martín Trejo</p>
    <p style="text-align:start">Mail: <a href="mailto:abogadomartingabrieltrejo@gmail.com" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">abogadomartingabrieltrejo@gmail.com</a></p>
    <p style="text-align:start">Tel: <a href="tel:3424787195" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">3424787195</a></p>
  </li>
  <li>
    <p style="text-align:start">Cdor Rodrigo Olgado</p>
    <p style="text-align:start">Consultora SCE</p>
    <p style="text-align:start">Actividad: Estudio Contable</p>
    <p style="text-align:start">Mail: <a href="mailto:rodrigoolgado@sceconsultora.com.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">rodrigoolgado@sceconsultora.com.ar</a></p>
  </li>
</ul>