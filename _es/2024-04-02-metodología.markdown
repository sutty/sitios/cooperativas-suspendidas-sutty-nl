---
title: Metodología
description: "¿Cómo utilizamos la información disponible en este sitio?"
author: []
categories: []
tags: []
locales: []
draft: false
order: 3
layout: post
uuid: 5ba9749a-4394-461a-9cf0-0cb814331fad
render_with_liquid: false
usuaries:
- 1
- 5
created_at: 2024-04-02 16:38:32.752455584 +00:00
last_modified_at: 2024-04-03 11:54:26.086365170 +00:00
---

<p style="text-align:start">Para realizar esta herramienta nos basamos en una base de datos publicada oficialmente (anexos de las Resoluciones <a href="https://www.boletinoficial.gob.ar/detalleAviso/primera/305279/20240403" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">878/2024</a> y <a href="https://www.boletinoficial.gob.ar/detalleAviso/primera/305280/20240403" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">879/2024</a> publicadas el 3/4/2024).</p>
<p style="text-align:start">Listamos los procedimientos técnicos necesarios.</p>
<ul>
  <li>
    <p style="text-align:start">Extraer los datos de los anexos en PDF como CSV (<a href="https://cooperativas-suspendidas.sutty.nl/public/r11y6dfu7loswa0v84eahdjxot7b/base_completa.csv" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">descargable aquí</a>)</p>
  </li>
  <li>
    <p style="text-align:start">Corregir posibles errores de conversión, como nombres largos de cooperativas y mutuales.</p>
  </li>
  <li>
    <p style="text-align:start">Indexar los listados por nombre de la cooperativa, matrícula, CUIT (no siempre especificado) y anexo en el que aparece. Si el nombre dice "(suspendida)" se la considera suspendida.</p>
  </li>
  <li>
    <p style="text-align:start">El buscador busca una matrícula o CUIT específico. Si se usa parte del nombre busca en todo el nombre. Es posible que haya que intentar varias combinaciones o sin palabras comunes como "coop", mientras mejoramos la herramienta.</p>
  </li>
  <li>
    <p style="text-align:start">Dependiendo del anexo en el que estén, se cargan recomendaciones de pasos a seguir y vincula a un instructivo.</p>
  </li>
</ul>
<p style="text-align:start">La Asociación Civil TES elaboró cuidadosa y detalladamente un instructivo que es la base que posibilita esta herramienta, brindando una asistencia a mutuales y cooperativas para que puedan orientarse respecto de los pasos a seguir.</p>
<p style="text-align:start">Además, elaboramos un gráfico que permite visualizar el porcentaje de cooperativas suspendidas sobre el total listado. Aguardamos a confirmar datos de cooperativas totales para agregar.</p>
<p style="text-align:start">Creemos fervientemente en la transparencia de la información y abogamos por los datos abiertos, por lo que necesariamente usamos Software Libre y promovemos (y facilitamos su uso).</p>
<p style="text-align:start">Podrán descargar tanto los insumos como materiales y <a href="https://0xacab.org/sutty/sitios/cooperativas-suspendidas-sutty-nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">código del proyecto</a>.</p>