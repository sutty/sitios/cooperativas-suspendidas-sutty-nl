---
title: En los medios
description: Listado de medios que difundieron este sitio
author: []
categories: []
tags: []
locales: []
draft: false
order: 11
layout: post
uuid: 15e7ee00-a3c8-4188-ab98-b8257ed456d0
render_with_liquid: false
usuaries:
- 1
created_at: 2024-04-04 21:46:52.486283859 +00:00
last_modified_at: 2024-04-04 21:47:18.135911958 +00:00
---

<p style="text-align:start"><a href="https://ansol.com.ar/facttic-tes-plataforma-cooperativas/generales/servicios/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">FACTTIC y TES lanzaron una plataforma para asistir a cooperativas suspendidas por el Gobierno</a>-- Agencia de Noticias Solidarias (ANSOL)</p>
<p style="text-align:start"><a href="https://lmdiario.com.ar/contenido/448111/lanzan-una-plataforma-virtual-para-asistir-a-cooperativas-suspendidas-por-el-gob" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Lanzan una plataforma virtual para asistir a cooperativas suspendidas por el Gobierno</a> -- La Nueva Mañana</p>
<p style="text-align:start"><a href="https://somostelam.com.ar/noticias/politica/el-gobierno-oficializo-la-suspension-de-9000-cooperativas-y-peligran-otras-7000-matriculas/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">El Gobierno oficializó la suspensión de 9000 cooperativas y peligran otras 7000 matrículas</a> -- Somos Télam</p>
<p style="text-align:start"><a href="https://www.diariomardeajo.com.ar/2024/04/03/ante-el-anuncio-de-adorni-de-suspension-de-11-000-cooperativas-surge-la-solidaridad-de-la-comunidad-cooperativa/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Ante el anuncio de Adorni de suspensión de 11.000 cooperativas, surge la Solidaridad de la Comunidad Cooperativa</a> -- Diario Mar de Ajó</p>
<p style="text-align:start"><a href="https://www.lacapital.com.ar/la-ciudad/lanzan-una-plataforma-ayudar-las-cooperativas-y-mutuales-suspendidas-el-gobierno-n10126737.html" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Lanzan una plataforma para ayudar a las cooperativas y mutuales suspendidas por el gobierno</a> -- La Capital</p>
<p style="text-align:start"><a href="https://www.pagina12.com.ar/726405-lazos-cooperativos-para-enfrentar-la-suspension-impuesta-por" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Lazos cooperativos para enfrentar la suspensión impuesta por Milei</a> -- Página/12</p>
<p style="text-align:start"><a href="https://www.tiempoar.com.ar/ta_article/cooperativas-suspendidas-web/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Cooperativas suspendidas: verdades a medias del gobierno y una web como respuesta al ataque</a> -- Tiempo Argentino</p>
<p style="text-align:start"></p>