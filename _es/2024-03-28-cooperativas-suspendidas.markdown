---
title: Cooperativas suspendidas
tagline: Buscador de cooperativas y mutuales suspendidas por las resoluciones 878/2024, 879/2024 y 3208/2024
description: Buscador de cooperativas y mutuales suspendidas por las resoluciones
  878/2024, 879/2024 y 3208/2024
logo:
  path: public/z3cjq94r1h8ga9ksxnem4sbhik3m/facttic.jpg
  description: FACTTIC
social_networks: []
locales: []
draft: false
order: 1
layout: about
uuid: d9263a7b-2156-43d1-b6ae-7a6656dabab9
render_with_liquid: false
usuaries:
- 1
created_at: 2024-03-28 19:46:00.629057079 +00:00
last_modified_at: 2024-03-28 19:46:00.632938769 +00:00
---

