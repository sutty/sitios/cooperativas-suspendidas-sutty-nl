---
title: Gacetilla de prensa
description: 'Suspensión de cooperativas: La Federación Argentina de Cooperativas
  de Trabajo de Tecnología, Innovación y Conocimiento (FACTTIC) y la Asociación Civil
  Trabajando por la Economía Social (TES) lanzaron una plataforma de asistencia solidaria
  para cooperativas afectadas por la medida.'
author:
- FACTTIC
categories: []
tags: []
locales: []
draft: false
order: 5
layout: post
uuid: c0e011c7-b967-42d7-a55f-2a29ec74b158
render_with_liquid: false
usuaries:
- 1
- 5
created_at: 2024-04-02 16:39:25.855732823 +00:00
last_modified_at: 2024-04-03 11:51:15.597356156 +00:00
---

<p style="text-align:start">A raíz de las declaraciones del Vocero Presidencial, Manuel Adorni, quien el 27 de marzo se refirió a la suspensión de 11.000 cooperativas e intimación de las mismas por supuestas irregularidades, en tiempo récord llega una respuesta solidaria de la mano de la misma comunidad cooperativa.</p>
<p style="text-align:start">Se trata de una plataforma en línea, accesible desde cualquier navegador, que permite buscar cooperativas y mutuales por su nombre o razón social, CUIT o matrícula. El resultado arroja rápidamente si se encuentra afectada por la medida anunciada y recomienda pasos a seguir para poder continuar operando, así como canales de asistencia.</p>
<p style="text-align:start">El sitio fue realizado por la cooperativa Sutty, parte de FACTTIC y con la participación de la Federación, ante la iniciativa de TES, quien realizó los informes e instructivos, además de contar con los listados, actualizados tras su publicación en el Boletín Oficial (3/4/24).</p>
<p style="text-align:start">Además de brindar el servicio a las cooperativas y mutuales, al tratarse de un proyecto de Software Libre y transparencia de datos, se puede encontrar, junto con su metodología y un breve análisis de los resultados, los datos disponibles para su descarga para su procesamiento por parte de quienes tengan interés, así como su código fuente.</p>
<p style="text-align:start">Frente a los dichos del vocero presidencial, desde FACTTIC cuestionan este tipo de medidas como parte de un intento de denostar el sector cooperativo y mutual que motoriza la economía y genera miles de puestos de trabajo.</p>
<p style="text-align:start">“Desde nuestra federación creemos que cada cooperativa es una herramienta que permite generar trabajo, producir y desarrollar la economía argentina. En este sentido, afirmamos que es fundamental sostener y fomentar el movimiento cooperativo y mutual, no sólo en defensa de sus principios solidarios y humanitarios, sino porque beneficia a la sociedad argentina en términos productivos.” Declaro Manuel Leiva, Presidente de FACTTIC.</p>
<p style="text-align:start">En números el cooperativismo y mutualismo argentino representa:</p>
<ul>
  <li>
    <p style="text-align:start">El 15% del PBI argentino.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">4 mil millones de dólares de exportación anuales.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">400 mil puestos de trabajo.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">4 millones de personas atendidas en salud al año.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">8 millones de hogares con luz, gas y conectividad otorgadas por cooperativas.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">10 millones de personas accediendo a alimentos más baratos.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">21,2% del mercado asegurador.</p>
  </li>
</ul>
<ul>
  <li>
    <p style="text-align:start">24% del mercado sobre entidades financieras.</p>
  </li>
</ul>
<p style="text-align:start">Argentina se encuentra a la vanguardia del movimiento cooperativo mundial, de manera que atacarlo implica también poner en riesgo este logro y atacar nuestra identidad.</p>
<p style="text-align:start">“Una cooperativa más, es una posibilidad de que un grupo de amigues, emprendedorxs y trabajadorxs puedan cumplir su sueño. Es permitir que se desarrolle la economía y fortalecer nuestro país por una simple razón: las cooperativas, por ley, no pueden mudarse ni especular con cambiarse de país. Si a la Argentina le va bien, a las cooperativas nos va a ir bien.” Afirmó Leiva.</p>
<p style="text-align:start">Link a la plataforma: https://cooperativas-suspendidas.sutty.nl/</p>
<p style="text-align:start">Contacto de prensa:</p>
<p style="text-align:start">Manuel Leiva (Presidente de FACTTIC): 3364662755</p>